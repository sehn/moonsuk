from django.contrib import admin
from django.urls import path, include
from . import views
urlpatterns = [   
    path('company/', views.company),
    path('main/', views.main),
    path('culture/', views.culture),
    path('career/', views.career),
    path('how_we_hire/', views.how_we_hire),
    path('what_we_do_0/', views.what_we_do_0),
    path('register/', views.register),
    path('notice/', views.notice),
    path('admin-login/', views.admin_login),
    path('admin-notice/', views.admin_notice),
    path('admin_career_now', views.admin_career_now),
    path('career_detail', views.career_detail),
    path('admin_popup', views.admin_popup),
    path('admin_new_career', views.admin_new_career),
    path('admin_new_notice', views.admin_new_notice),
    path('admin_notice_detail', views.admin_notice_detail),
path('admin_popup_detail', views.admin_popup_detail),
]
