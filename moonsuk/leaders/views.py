from django.shortcuts import render

# Create your views here.
def company(request):
    return render(request, "leaders/company.html")
def main(request):
    return render(request, "leaders/main.html")
def culture(request):
    return render(request, "leaders/culture.html")
def career(request):
    return render(request, "leaders/career.html")
def how_we_hire(request):
    return render(request, "leaders/how_we_hire.html")
def what_we_do_0(request):
    return render(request, "leaders/what_we_do_0.html")
def register(request):
    return render(request, "leaders/register.html")
def notice(request):
    return render(request, "leaders/notice.html")
def admin_login(request):
    return render(request, "leaders/admin_login.html")
def admin_notice(request):
    return render(request, "leaders/admin_notice.html")

def admin_career_now(request):
    return render(request, "leaders/admin_career_now.html")
def career_detail(request):
    return render(request, "leaders/career_detail.html")
def admin_popup(request):
    return render(request, "leaders/admin_popup.html")

def admin_new_career(request):
    return render(request, "leaders/admin_new_career.html")
def admin_new_notice(request):
    return render(request, "leaders/admin_new_notice.html")
def admin_notice_detail(request):
    return render(request, "leaders/admin_notice_detail.html")

def admin_popup_detail(request):
    return render(request, "leaders/admin_popup_detail.html")